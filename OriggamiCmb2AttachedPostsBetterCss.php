<?php

namespace OriggamiCmb2AttachedPostsBetterCss;

/**
 * Plugin Name: Origgami - Cmb2 Attached Posts - Better Css
 * Plugin URI: http://origgami.com.br
 * Description: Better CSS for plugin Cmb2 Attached Posts
 * Version: 1.0.0
 * Author: Origgami
 * Author URI: http://origgami.com.br
 * Text Domain: 
 * Domain Path: /i18n
 * Network: 
 * License: GPL2
 * Bitbucket Plugin URI: https://bitbucket.org/origgamiwordpressplugins/origgami-cmb2-attached-posts-better-css
 * Bitbucket Branch:     master
 */

/**
 * Description of Cmb2AttachedPostsOriggamiCss
 *
 * @author Pablo Pacheco <pablo.pacheco@origgami.com.br>
 */
class OriggamiCmb2AttachedPostsBetterCss {

	public function __construct() {
		add_action('admin_head', array($this, 'customCss'));
	}

	public function customCss() {
		?>
		<style>
			.retrieved-wrap.column-wrap:before{
				content:'Disponíveis para seleção';
				margin-bottom:5px;
				display:block;
			}
			.attached-wrap.column-wrap:before{
				content:'Selecionados'; 
				margin-bottom:5px;
				display:block;
			}
			.cmb-type-custom-attached-posts .has-thumbnails.connected{
				height:auto;
				max-height:300px !important;
			}
			.attached-posts-section{
				display:none !important;
			}
			.cmb-type-custom-attached-posts .retrieved-wrap{
				margin-right:2% !important;
			}
			.cmb-type-custom-attached-posts .column-wrap{
				width:49% !important;
			}
			.cmb-type-custom-attached-posts .has-thumbnails li img{
				width:35px !important;
				height:100% !important;
				float:none !important;
			}
			.cmb-type-custom-attached-posts .connected li:hover{
				//cursor:move;
			}

			.retrieved-wrap .retrieved{
				/*position:relative;*/
			}
			.retrieved-wrap .ui-draggable-dragging{
				/*width:100%;*/
				width:43%;
				display:block;
				z-index:3 !important;
				background:#fff;
			}
			.ui-draggable-handle{
				position:relative;
				z-index:2 !important;
			}
		</style>
		<?php

	}

}

new OriggamiCmb2AttachedPostsBetterCss();

